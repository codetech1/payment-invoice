`Technology Used:`

* Spring boot
* JPA
* Hibernate
* Rest API

`Database Used:`

* Mysql 8.0

`External Tool for Run APIs:`

* Postman

`For Build & Run Jar:`

* mvn clean install spring-boot:run


`Release Component:`

* Java project.
* Postman collection.
* Video for demonstrate the functionality.
* Readme file.
* Database Script (No need to run script if you are able to compile and run project , It will create a db).

`Note:` 
*In application.properties file you need to change database credentials where it mentioned as  xxxx.

 
