package com.payment.terms.services.model.request;

import java.util.Date;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)
public class PaymentRequest {

	private Long id;

	@NotBlank(message = "Please provide a code")
	private String code;

	@Size(max = 250, message="Please reduce description data below 250 chars")
	private String description;

	@NotNull(message = "Please provide a date in yyyy-mm-dd format")
	@JsonFormat(pattern = "yyyy-mm-dd")
	private Date creationDate;

	@NotNull(message = "Please provide days")
	private Integer days;

	@NotNull(message = "Please provide remindBeforeDays")
	private Integer remindBeforeDays;
}
