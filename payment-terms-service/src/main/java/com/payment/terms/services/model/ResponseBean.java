package com.payment.terms.services.model;

import java.util.Date;

import org.springframework.http.HttpStatus;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class ResponseBean {

	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss.SSSSSS")
	private String timestamp = String.valueOf(new Date());
	private Integer status;
	private String message;
	private Object details;
	private String path;
	
	public ResponseBean(){}
	
	public ResponseBean(int code,String path,Object details){
		this.setStatus(code);
		this.setMessage(getHttpStausMessage(code));
		this.setPath(path);
		this.setDetails(details);
	}
	
	public String getHttpStausMessage(int code) {
		if(code == 201)
			return HttpStatus.CREATED.name();
		return HttpStatus.OK.name();
	}
			
}
