package com.payment.terms.services.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.payment.terms.services.dto.PaymentDto;


@Repository
public interface PaymentRepository extends JpaRepository<PaymentDto, Long>{

	/**
	 * Payment object will get from database by using paymentTerm filter.
	 * 
	 * @param paymentTerm
	 * @return
	 */
	PaymentDto findByCode(String paymentTerm);

}
