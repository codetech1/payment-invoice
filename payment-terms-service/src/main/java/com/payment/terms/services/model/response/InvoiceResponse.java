package com.payment.terms.services.model.response;

import com.fasterxml.jackson.annotation.JsonAutoDetect;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)
@ToString
public class InvoiceResponse {

	private String invoiceNumber;
	
    private String invoiceDate;
	
    private String paymentTerm;
	
    private String status;
}
