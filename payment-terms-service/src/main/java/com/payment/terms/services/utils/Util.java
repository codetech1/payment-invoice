package com.payment.terms.services.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.lang3.time.DateUtils;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.context.request.WebRequest;

@Service
public class Util {

	private ModelMapper mapper = new ModelMapper();

	public <T> T convert(Object source, Class<T> destination) {
		if (source == null) {
			return null;
		}
		return mapper.map(source, destination);
	}

	public String getControllerUrlFromWebRequest(WebRequest request) {
		String prepareURl = ((ServletWebRequest) request).getRequest().getRequestURI().toString();
		return prepareURl.replace(request.getContextPath(), "");
	}
	
	
	public Date addDays(String date, Integer addDaysCount) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Date formatedDate = null;
		try {
			formatedDate = sdf.parse(date);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		//Number of Days to add
	    return DateUtils.addDays(formatedDate, addDaysCount);
	}
		
	public Boolean compareTwoDates(Date startDate, Date endDate) {
		Date sDate = getZeroTimeDate(startDate);
		Date eDate = getZeroTimeDate(endDate);

		if (eDate.before(sDate)) {
			return true;
		}
		if (sDate.equals(eDate)) {
			return true;
		}
		return false;
	}
	
	public Date getZeroTimeDate(Date date) {
		date.setHours(0);
		date.setMinutes(0);
		date.setSeconds(0);
		return date;
	}
	
}
