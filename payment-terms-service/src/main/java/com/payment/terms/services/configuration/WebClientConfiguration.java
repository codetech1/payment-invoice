package com.payment.terms.services.configuration;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.client.reactive.ClientHttpConnector;
import org.springframework.http.client.reactive.ReactorClientHttpConnector;
import org.springframework.web.reactive.function.client.WebClient;

import io.netty.channel.ChannelOption;
import io.netty.handler.timeout.ReadTimeoutHandler;
import io.netty.handler.timeout.WriteTimeoutHandler;
import reactor.netty.http.client.HttpClient;

@Configuration
public class WebClientConfiguration {
    @Value("${invoice-web.url}")
    private String baseUri;

    @Value("${invoice-web.timeout}")
    private int timeout;

    @Bean
    public WebClient webClient()  {
        return WebClient.builder()
                .baseUrl(baseUri)
                .clientConnector(getConnector())
                .defaultHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .build();
    }

    private ClientHttpConnector getConnector()  {
        return new ReactorClientHttpConnector(getHttpClient());
    }

    private HttpClient getHttpClient()  {
    	return HttpClient.create()      
                .tcpConfiguration(client ->
                        client.option(ChannelOption.CONNECT_TIMEOUT_MILLIS, timeout)
                                .doOnConnected(conn -> conn
                                        .addHandlerLast(new ReadTimeoutHandler(30))
                                        .addHandlerLast(new WriteTimeoutHandler(30))));
    }
}
