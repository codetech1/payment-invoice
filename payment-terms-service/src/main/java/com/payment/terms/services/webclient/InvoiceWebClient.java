package com.payment.terms.services.webclient;

import java.util.Arrays;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.payment.terms.services.model.response.InvoiceResponse;

import reactor.core.publisher.Mono;

@Component
public class InvoiceWebClient extends AbstractWebClient {

	@Value("${invoice-web.url}")
	private String invoiceWebUrl;
	
	private static final Logger logger = LogManager.getLogger(InvoiceWebClient.class);

	/* Method is used for access token is valid or not */
	public List<InvoiceResponse> fetchUnpaidInvocies() {
		Mono<InvoiceResponse[]> response = get(invoiceWebUrl, InvoiceResponse[].class);
		logger.trace("fetchUnpaidInvocies response: {}", response);
		return Arrays.asList(response.block());
	}
}
