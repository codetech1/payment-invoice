package com.payment.terms.services.service;

import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.payment.terms.services.dto.PaymentDto;
import com.payment.terms.services.enums.ErrorMessageEnums;
import com.payment.terms.services.exception.RecordConflictException;
import com.payment.terms.services.exception.RecordNotFoundException;
import com.payment.terms.services.model.request.PaymentRequest;
import com.payment.terms.services.model.response.InvoiceResponse;
import com.payment.terms.services.model.response.PaymentReponse;
import com.payment.terms.services.repository.PaymentRepository;
import com.payment.terms.services.service.api.PaymentService;
import com.payment.terms.services.utils.Util;



@Service
public class PaymentServiceImpl implements PaymentService{

	private PaymentRepository paymentRepository;
	private Util utilService;
	
	
	@Autowired
	public PaymentServiceImpl(PaymentRepository paymentRepository,Util utilService) {
		this.paymentRepository = paymentRepository;
		this.utilService = utilService;
	}
	

	/**
	 * Method is used for save the record in database using PaymentRequest object
	 * 
	 * @param request
	 * @return PaymentReponse
	 */
	@Override
	public PaymentReponse save(PaymentRequest request) {
		PaymentDto requestPayment = utilService.convert(request, PaymentDto.class);
		if(requestPayment.getId() != null) {
			validate(requestPayment.getId());
		}
		if(paymentRepository.findByCode(requestPayment.getCode()) != null) {
			throw new RecordConflictException(ErrorMessageEnums.RECORD_CONFLCT.toString());
		}
		return utilService.convert(paymentRepository.save(requestPayment), PaymentReponse.class);
	}
	
	private void validate(Long id) {
		Optional<PaymentDto> fetchedPayment = paymentRepository.findById(id);
		if(!fetchedPayment.isPresent()) {
			throw new RecordNotFoundException(ErrorMessageEnums.RECORD_NOT_FOUND.toString());
		}
	}

	/**
	 * Method is used for fetch all the records from database
	 * 
	 * @return List<PaymentReponse>
	 */
	@Override
	public List<PaymentReponse> getAll() {
		return paymentRepository.findAll().stream()
				.map(payemntDto->{return utilService.convert(payemntDto, PaymentReponse.class);})
				.collect(Collectors.toList());
	}

	/**
	 * Method is used for fetch record by ID from database
	 * 
	 * @param paymentID
	 * @return PaymentReponse
	 */
	@Override
	public PaymentReponse findById(Long paymentID) {
		Optional<PaymentDto> fetchedPayment = paymentRepository.findById(paymentID);
		if(!fetchedPayment.isPresent()) {
			throw new RecordNotFoundException(ErrorMessageEnums.RECORD_NOT_FOUND.toString());
		}
		return utilService.convert(fetchedPayment.get(), PaymentReponse.class);
	}

	/**
	 * Method is used for delete record by ID from database
	 * 
	 * @param paymentID
	 */
	@Override
	public void delete(Long paymentID) {
		validate(paymentID);
		paymentRepository.deleteById(paymentID);
	}

	/**
	 * Method is used to check the date is in reminder range or not.
	 * 
	 * @param unpaidInvoice
	 * @return
	 */
	@Override
	public Boolean checkAndvalidateDateForReminder(InvoiceResponse unpaidInvoice) {
		PaymentDto fetchedPayment =  paymentRepository.findByCode(unpaidInvoice.getPaymentTerm());
		if(fetchedPayment != null) {
			Date reminderDate = utilService.addDays(unpaidInvoice.getInvoiceDate(), fetchedPayment.getDays() - fetchedPayment.getRemindBeforeDays());
			return utilService.compareTwoDates(new Date(), reminderDate);
		}else {
			return false;
		}
	}
	
	
}
