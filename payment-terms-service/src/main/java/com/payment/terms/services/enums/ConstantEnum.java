package com.payment.terms.services.enums;

public enum ConstantEnum {

	UNPAID("UNPAID"),
	PAID("PAID");

	private final String message;

	ConstantEnum(final String message) {
		this.message = message;
	}

	@Override
	public String toString() {
		return message;
	}

}
