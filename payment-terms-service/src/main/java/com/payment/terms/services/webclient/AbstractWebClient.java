package com.payment.terms.services.webclient;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.web.reactive.function.client.WebClient;

import reactor.core.publisher.Mono;

public abstract class AbstractWebClient {

	@Autowired
	private WebClient webClient;

	
	protected <S, T> Mono<T> get(String url, Class<T> response) {
		return webClient.get()
		        .uri(url)
		        .retrieve()
		        .bodyToMono(response);
	}

}
