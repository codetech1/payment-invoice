package com.payment.terms.services.exception;


import java.util.List;
import java.util.stream.Collectors;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.payment.terms.services.model.ErrorResponse;
import com.payment.terms.services.utils.Util;

@ControllerAdvice
public class CustomExpcetionHandler extends ResponseEntityExceptionHandler {

    private static final Logger logger = LogManager.getLogger(CustomExpcetionHandler.class);
    
    private Util utilService;
        
    @Autowired
    public CustomExpcetionHandler(Util utilService) {
		this.utilService = utilService;
	}

	@ExceptionHandler(RecordNotFoundException.class)
    public final ResponseEntity<Object> handleRecordNotFoundException(RecordNotFoundException ex, WebRequest request) {
        String details = ex.getLocalizedMessage();
        ErrorResponse errorresponse = new ErrorResponse(404,
                utilService.getControllerUrlFromWebRequest(request), "Record Not Found", details);
        logger.error("Record Not Found", errorresponse);
        return new ResponseEntity<Object>(errorresponse, HttpStatus.NOT_FOUND);
    }
    
    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
                                                                  HttpHeaders headers, HttpStatus status, WebRequest request) {
        // Get all errors
        List<String> errors = ex.getBindingResult().getFieldErrors().stream().map(x -> x.getDefaultMessage())
                .collect(Collectors.toList());
        ErrorResponse errorresponse = new ErrorResponse(400,
                utilService.getControllerUrlFromWebRequest(request), "Validation Failed", errors);
        return new ResponseEntity<Object>(errorresponse, headers, status);
    }
    
    @ExceptionHandler(RecordConflictException.class)
    public final ResponseEntity<Object> handleRecordConflictException(RecordConflictException ex, WebRequest request) {
    	String details = ex.getLocalizedMessage();
        ErrorResponse errorresponse = new ErrorResponse(409,
                utilService.getControllerUrlFromWebRequest(request), "Record Conflicts", details);
        return new ResponseEntity<Object>(errorresponse, HttpStatus.CONFLICT);
    }
   

    @ExceptionHandler(Exception.class)
    public final ResponseEntity<Object> exceptionHandler(Exception ex, WebRequest request) {
        String details = ex.getLocalizedMessage();
        ErrorResponse errorresponse = new ErrorResponse(500,
                utilService.getControllerUrlFromWebRequest(request), "Server Error", details);        
        return new ResponseEntity<Object>(errorresponse, HttpStatus.INTERNAL_SERVER_ERROR);
    }

}
