package com.payment.terms.services.job;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.payment.terms.services.model.response.InvoiceResponse;
import com.payment.terms.services.service.api.PaymentService;
import com.payment.terms.services.webclient.InvoiceWebClient;



@Component
public class SchedulerJob {

	private static final Logger logger = LogManager.getLogger(SchedulerJob.class);
	
	private PaymentService paymentService;
	private InvoiceWebClient invoiceWebClient;
	
	@Autowired	
	public SchedulerJob(PaymentService paymentService, InvoiceWebClient invoiceWebClient) {		
		this.paymentService = paymentService;
		this.invoiceWebClient=invoiceWebClient;
	}	
	
	@Scheduled(cron = "${schedule.duration.string}")
	public void trackOverduePayments() {
		logger.info("Started In - TrackOverduePayments used for sent Reminder for Invoices..");
		List<InvoiceResponse> unpaidInvoices = invoiceWebClient.fetchUnpaidInvocies();
		for(InvoiceResponse unpaidInvoice : unpaidInvoices) {
			System.out.println(unpaidInvoice);
			if(paymentService.checkAndvalidateDateForReminder(unpaidInvoice)) {
				logger.info("Reminder sent for Invoice "+unpaidInvoice.getInvoiceNumber());
			}			
		}
        logger.info("End In - TrackOverduePayments used for sent Reminder for Invoices..");
    }
}
