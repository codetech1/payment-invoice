
--
-- Table structure for table `invoice`
--

DROP TABLE IF EXISTS `invoice`;
CREATE TABLE `invoice` (
  `invoice_number` varchar(255) NOT NULL,
  `invoice_date` datetime DEFAULT NULL,
  `payment_term` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`invoice_number`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `invoice`
--

LOCK TABLES `invoice` WRITE;
INSERT INTO `invoice` VALUES ('INV-001','2017-11-10','NET 30','PAID'),('INV-002','2020-12-25','NET 45','UNPAID'),('INV-003','2021-01-15','NET 30','UNPAID'),('INV-004','2021-01-23','NET 15','UNPAID');
UNLOCK TABLES;

--
-- Table structure for table `invoice_number_seq`
--

DROP TABLE IF EXISTS `invoice_number_seq`;
CREATE TABLE `invoice_number_seq` (
  `next_val` bigint DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `invoice_number_seq`
--

LOCK TABLES `invoice_number_seq` WRITE;
INSERT INTO `invoice_number_seq` VALUES (1);
UNLOCK TABLES;