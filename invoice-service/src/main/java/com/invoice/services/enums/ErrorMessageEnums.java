package com.invoice.services.enums;

public enum ErrorMessageEnums {

	RECORD_NOT_FOUND("Page or record could not found, please check request filters or url"),
	RECORD_CONFLCT("Record conflict, requested data is already saved in the system");

	private final String message;

	ErrorMessageEnums(final String message) {
		this.message = message;
	}

	@Override
	public String toString() {
		return message;
	}

}
