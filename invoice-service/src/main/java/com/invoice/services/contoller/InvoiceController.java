package com.invoice.services.contoller;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.invoice.services.model.response.InvoiceResponse;
import com.invoice.services.service.api.InvoiceService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Api(tags = ("InvoiceController"))
@RestController
@RequestMapping("/invoices")
public class InvoiceController {

	
	private static final Logger logger = LogManager.getLogger(InvoiceController.class);
	
	private InvoiceService invoiceService;
	
	@Autowired
	public InvoiceController(InvoiceService invoiceService) {
		this.invoiceService = invoiceService;
	}
	
	/**
	 * Method is used for fetch all Unpaid invoices from database
	 * 
	 * @return List<InvoiceResponse>
	 */
	@ApiOperation(value = "Get All Unpaid Invoices", notes = "Method is used for fetch all Unpaid invoices from database")
	@GetMapping
    public ResponseEntity<?> getPayments() {
		logger.info("Started into {} getPayments", "getPayments");
		List<InvoiceResponse> responses = invoiceService.getAllUnPaidInvoices();
		logger.info("End into {} getPayments",responses);
		return ResponseEntity.ok(responses);
    }
	
	
	
}
