package com.invoice.services.model;



import java.util.Date;

import org.springframework.http.HttpStatus;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class ErrorResponse {
	
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss.SSSSSS")
	private String timestamp = String.valueOf(new Date());
	private Integer status;
	private String error;
	private String message;
	private Object details;
	private String path;
	
	public ErrorResponse(int code, String path, String message, Object details){
		this.setStatus(code);
		this.setError(getHttpStausMessage(code));
		this.setPath(path);
		this.setMessage(message);
		this.setDetails(details);
	}

	public String getHttpStausMessage(int code) {
		 String result;
		    switch (code) {
		        case 400:
		            result = HttpStatus.BAD_REQUEST.name(); 
		            break;
		        case 401:
		            result = HttpStatus.UNAUTHORIZED.name();
		            break;
		        case 403:
		            result = HttpStatus.FORBIDDEN.name();
		            break;
		        case 404:
		            result = HttpStatus.NOT_FOUND.name(); 
		            break;
		        case 405:
		            result = HttpStatus.METHOD_NOT_ALLOWED.name();
		            break;
		        case 409:
		            result = HttpStatus.CONFLICT.name();
		            break;
		        case 500:
		            result = HttpStatus.INTERNAL_SERVER_ERROR.name();
		            break;   
		        default:
		            result = HttpStatus.INTERNAL_SERVER_ERROR.name();
		            break;
		    }
		    return result;
	}

}
