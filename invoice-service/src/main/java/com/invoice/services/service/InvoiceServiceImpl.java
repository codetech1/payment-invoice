package com.invoice.services.service;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.invoice.services.enums.ConstantEnum;
import com.invoice.services.model.response.InvoiceResponse;
import com.invoice.services.repository.InvoiceRepository;
import com.invoice.services.service.api.InvoiceService;
import com.invoice.services.utils.Util;

@Service
public class InvoiceServiceImpl implements InvoiceService{
	
	private InvoiceRepository invoiceRepository;
	private Util utilService;
	
	@Autowired
	public InvoiceServiceImpl(InvoiceRepository invoiceRepository,Util utilService) {
		this.invoiceRepository = invoiceRepository;
		this.utilService = utilService;
	}

	@Override
	public List<InvoiceResponse> getAllUnPaidInvoices() {
		return invoiceRepository.findByStatus(ConstantEnum.UNPAID.toString()).stream()
				.map(invoice->{return utilService.convert(invoice, InvoiceResponse.class);})
				.collect(Collectors.toList());
	}
	
	
	
}
