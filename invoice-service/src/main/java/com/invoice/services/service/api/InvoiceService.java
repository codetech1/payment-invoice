package com.invoice.services.service.api;

import java.util.List;

import com.invoice.services.model.response.InvoiceResponse;

public interface InvoiceService {

	List<InvoiceResponse> getAllUnPaidInvoices();
}
