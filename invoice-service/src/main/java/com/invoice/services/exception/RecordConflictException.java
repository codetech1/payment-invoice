package com.invoice.services.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.CONFLICT)
public class RecordConflictException extends RuntimeException{

	private static final long serialVersionUID = 3884558620449975856L;

	 public RecordConflictException(String exception) {
	        super(exception);
	    }
}
